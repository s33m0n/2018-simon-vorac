//
//  BusinessCardContent.swift
//  EMAContacts
//
//  Created by No Name on 27/03/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

class BusinessCardRemoteContent: Decodable {
    let id: Int
    let name: String
    let icon: String
    let imageUrl: String?
    let slack_id: String
    let email: String
    let phone: String
    let position: String
    let scores: [Score]
    
    init(id: Int, name: String, icon: String, imageUrl: String?, slack_id: String, email: String, phone: String, position: String, scores: [Score]) {
        self.id = id
        self.name = name
        self.icon = icon
        self.imageUrl = imageUrl
        self.slack_id = slack_id
        self.email = email
        self.phone = phone
        self.position = position
        self.scores = scores
    }
}
