//
//  BusinessCardVMType.swift
//  EMAContacts
//
//  Created by No Name on 17/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

protocol BusinessCardVMType {
    var content: BusinessCardRemoteContent? { get set }
    var participantId: Int { get set }
    
    var gotContent: ((BusinessCardRemoteContent) -> Void)? { get set }
    var gotNetworkError: ((Error?) -> Void)? { get set }
        
    func loadBusinessCard()
}
