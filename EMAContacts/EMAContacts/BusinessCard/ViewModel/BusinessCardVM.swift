//
//  BusinessCardVM.swift
//  EMAContacts
//
//  Created by No Name on 17/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

class BusinessCardVM: BusinessCardVMType {
    
    var gotContent: ((BusinessCardRemoteContent) -> Void)?
    var gotNetworkError: ((Error?) -> Void)?
    
    var participantId: Int
    var content: BusinessCardRemoteContent?
    private let dataProvider: RemoteDataProviderType
    
    init(id: Int) {
        self.participantId = id
        self.dataProvider = RemoteDataProvider()
    }
    
    func loadBusinessCard() {
        dataProvider.loadParticipant(id: participantId) { [weak self] (content, error) in
            guard let content = content else {
                DispatchQueue.main.async {
                    print(error?.localizedDescription ?? "error")
                    self?.gotNetworkError?(error)
                }
                return
            }
            self?.gotContent?(content)
        }
    }
}
