//
//  BusinessCardScrollView.swift
//  EMAContacts
//
//  Created by No Name on 27/03/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import UIKit
import Kingfisher

private let displayPadding: CGFloat = 30
private let buttonHeight: CGFloat = 44
private let spacing: (low: CGFloat, mid: CGFloat, high: CGFloat) = (10, 20, 40)
private let fontSize: (medium: CGFloat, large: CGFloat, xLarge: CGFloat) = (14, 18, 24)

class BusinessCardRemoteScrollView: UIScrollView  {
    
    var dismiss: (() -> Void)?

    // MARK: - Variables
    // MARK: public
    
    var content: BusinessCardRemoteContent? {
        didSet {
            if let content = content {
                setupContent(content)
            } else {
                setupEmptyContent()
            }
        }
    }
    
    var logoutButton: UIButton?
    
    // MARK: private
    
    private lazy var loadingView: UIActivityIndicatorView = self.makeLoadingView()
    private lazy var stackView: UIStackView = UIStackView()
    lazy var avatarImageView: UIImageView = self.makeAvatarImageView()
    private lazy var nameLabel: UILabel = self.makeNameLabel()
    private lazy var slackButton: UIButton = self.makeSlackButton()
    private lazy var emailButton: UIButton = self.makeEmailButton()
    private lazy var phoneButton: UIButton = self.makePhoneButton()
    private lazy var positionLabel: UILabel = self.makePositionLabel()
    private lazy var separator: UIView = self.makeSeparatorView()
    private lazy var scoresViews: [ScoreView] = self.makeScoresViews()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        avatarImageView.addGestureRecognizer(tapGesture)
        avatarImageView.isUserInteractionEnabled = true
    }
    
    @objc func imageTapped() {
        avatarImageView.rotedAnimation(withDuration: 1, withAngle: 400, withRepetation: false, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Content
    // MARK: private
    
    private func setupEmptyContent() {
        subviews.forEach {
            $0.removeFromSuperview()
        }
        addSubview(loadingView)
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        loadingView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        loadingView.startAnimating()
    }
    
    private func setupContent(_ content: BusinessCardRemoteContent) {
        
        if stackView.superview == nil {
            let button = makeLogoutButton(title: "Odhlásit")
            addSubview(button)
            
            self.logoutButton = button
            
            let contactStackView = UIStackView()
            contactStackView.backgroundColor = .red
            contactStackView.alignment = .center
            contactStackView.axis = .horizontal
            contactStackView.spacing = spacing.low
            
            [slackButton, emailButton, phoneButton].forEach {
                contactStackView.addArrangedSubview($0)
            }
            
            addSubview(stackView)
            stackView.alignment = .center
            stackView.axis = .vertical
            stackView.spacing = spacing.mid
            
            [avatarImageView, nameLabel, contactStackView, positionLabel, separator].forEach {
                stackView.addArrangedSubview($0)
            }
            scoresViews.forEach {
                stackView.addArrangedSubview($0)
                stackView.setCustomSpacing(spacing.low, after: $0)
            }
            stackView.setCustomSpacing(spacing.high, after: nameLabel)
            
            setupConstratins()
        }
        updateContent(content)
    }
    
    private func setupConstratins() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: topAnchor, constant: 2 * spacing.high + 40).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        
        logoutButton?.anchor(top: self.topAnchor, leading: nil, bottom: nil, trailing: self.trailingAnchor, centerX: nil, centerY: nil, padding: .init(top: 40, left: 0, bottom: 0, right: -20), size: .init(width: 100, height: 44))
        
        avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        avatarImageView.widthAnchor.constraint(equalToConstant: 2 * avatarImageView.layer.cornerRadius).isActive = true
        avatarImageView.heightAnchor.constraint(equalTo: avatarImageView.widthAnchor).isActive = true
        
        separator.translatesAutoresizingMaskIntoConstraints = false
        separator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separator.widthAnchor.constraint(equalTo: stackView.widthAnchor, constant: -2 * displayPadding).isActive = true
        
        scoresViews.forEach { rowView in
            rowView.translatesAutoresizingMaskIntoConstraints = false
            rowView.heightAnchor.constraint(equalToConstant: buttonHeight).isActive = true
            rowView.widthAnchor.constraint(equalTo: stackView.widthAnchor, constant: -2 * displayPadding).isActive = true
        }
    }
    
    private func updateContent(_ content: BusinessCardRemoteContent) {
        
        //Get participant's image from url and set it using Kingfisher
        guard let imageUrl = content.imageUrl else { return }
        let url = URL(string: imageUrl)
        avatarImageView.kf.setImage(with: url, completionHandler: { [weak self] (_, _, _, _) in
            self?.setNeedsLayout()
        })
        
        nameLabel.text = content.name
        positionLabel.text = content.position
        zip(scoresViews, content.scores).forEach {
            $0.0.content = $0.1
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        [slackButton, emailButton, phoneButton].forEach {
            $0.centerVertically()
        }
    }
    
    // MARK: Views builders
    
    private func makeLogoutButton(title: String) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        button.layer.cornerRadius = 6
        button.backgroundColor = UIColor(named: "academy")
        button.addTarget(self, action: #selector(logoutButtonPressed), for: .touchUpInside)
        return button
    }
    
    @objc func logoutButtonPressed() {
        print("logged out succesfully")
        dismissView()
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
    }
    
    @objc func dismissView() {
        dismiss?()
    }
    
    private func makeLoadingView() -> UIActivityIndicatorView {
        let rVal = UIActivityIndicatorView()
        rVal.activityIndicatorViewStyle = .gray
        return rVal
    }
    
    private func makeAvatarImageView() -> UIImageView {
        let rVal = UIImageView()
        rVal.contentMode = .scaleAspectFit
        rVal.layer.borderColor = UIColor.black.withAlphaComponent(0.1).cgColor
        rVal.layer.borderWidth = 1
        rVal.layer.cornerRadius = 75
        return rVal
    }
    
    private func makeNameLabel() -> UILabel {
        let rVal = UILabel()
        rVal.font = .systemFont(ofSize: fontSize.xLarge)
        return rVal
    }
    
    private func makeButton() -> UIButton {
        let rVal = UIButton()
        rVal.setTitleColor(.black, for: .normal)
        rVal.setTitleColor(UIColor.black.withAlphaComponent(0.6), for: .highlighted)
        rVal.titleLabel?.font = .systemFont(ofSize: fontSize.medium)
        return rVal
    }
    
    private func makeSlackButton() -> UIButton {
        let rVal = makeButton()
        rVal.setTitle("Slack", for: .normal)
        rVal.setImage(UIImage(named: "ic-slack"), for: .normal)
        return rVal
    }
    
    private func makeEmailButton() -> UIButton {
        let rVal = makeButton()
        rVal.setTitle("E-mail", for: .normal)
        rVal.setImage(UIImage(named: "ic-email"), for: .normal)
        return rVal
    }
    
    private func makePhoneButton() -> UIButton {
        let rVal = makeButton()
        rVal.setTitle("Telefon", for: .normal)
        rVal.setImage(UIImage(named: "ic-phone"), for: .normal)
        return rVal
    }
    
    private func makePositionLabel() -> UILabel {
        let rVal = UILabel()
        rVal.font = .boldSystemFont(ofSize: fontSize.large)
        return rVal
    }
    
    private func makeSeparatorView() -> UIView {
        let rVal = UIView()
        rVal.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        return rVal
    }
    
    private func makeScoresViews() -> [ScoreView] {
        return content?.scores.map { _ in
            ScoreView()
            } ?? []
    }
}

//private class ScoreView: UIView {
//    // MARK: - Variables
//    // MARK: public
//    
//    var content: Score? {
//        didSet {
//            setupContent()
//        }
//    }
//    
//    // MARK: private
//    
//    private lazy var leftLabel: UILabel = self.makeLeft()
//    private lazy var rightLabel: UILabel = self.makeRight()
//    
//    // MARK: Content
//    
//    private func setupContent() {
//        if subviews.isEmpty {
//            addSubview(leftLabel)
//            addSubview(rightLabel)
//            
//            leftLabel.translatesAutoresizingMaskIntoConstraints = false
//            leftLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
//            leftLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
//            leftLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
//            
//            rightLabel.translatesAutoresizingMaskIntoConstraints = false
//            rightLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
//            rightLabel.leadingAnchor.constraint(equalTo: leftLabel.trailingAnchor).isActive = true
//            rightLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
//            rightLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
//        }
//        updateContent()
//    }
//    
//    private func updateContent() {
//        leftLabel.text = content?.name ?? ""
//        rightLabel.text = String(content?.value ?? 0)
//    }
//    
//    private func makeLeft() -> UILabel {
//        let rVal = UILabel()
//        rVal.font = .systemFont(ofSize: fontSize.medium)
//        return rVal
//    }
//    
//    private func makeRight() -> UILabel {
//        let rVal = UILabel()
//        rVal.font = .boldSystemFont(ofSize: fontSize.medium)
//        rVal.textAlignment = .right
//        return rVal
//    }
//
//}
