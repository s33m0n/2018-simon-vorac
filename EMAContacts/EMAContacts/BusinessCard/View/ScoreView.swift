//
//  ScoreView.swift
//  EMAContacts
//
//  Created by No Name on 04/05/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import UIKit

public class ScoreView: UIView {
    // MARK: - Variables
    // MARK: public
    
    var content: Score? {
        didSet {
            setupContent()
        }
    }
    
    // MARK: private
    
    private lazy var leftLabel: UILabel = self.makeLeft()
    private lazy var rightLabel: UILabel = self.makeRight()
    
    // MARK: Content
    
    private func setupContent() {
        if subviews.isEmpty {
            addSubview(leftLabel)
            addSubview(rightLabel)
            
            leftLabel.translatesAutoresizingMaskIntoConstraints = false
            leftLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
            leftLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            leftLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            
            rightLabel.translatesAutoresizingMaskIntoConstraints = false
            rightLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
            rightLabel.leadingAnchor.constraint(equalTo: leftLabel.trailingAnchor).isActive = true
            rightLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            rightLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        }
        updateContent()
    }
    
    private func updateContent() {
        leftLabel.text = content?.name ?? ""
        rightLabel.text = String(content?.value ?? 0)
    }
    
    private func makeLeft() -> UILabel {
        let rVal = UILabel()
        rVal.font = .systemFont(ofSize: 14)
        return rVal
    }
    
    private func makeRight() -> UILabel {
        let rVal = UILabel()
        rVal.font = .boldSystemFont(ofSize: 14)
        rVal.textAlignment = .right
        return rVal
    }
    
}

