//
//  BusinessCardRemoteViewController.swift
//  EMAContacts
//
//  Created by No Name on 10/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import UIKit

class BusinessCardRemoteViewController: UIViewController, SkipOverlayDelegate {
    
    var overlay: GDOverlay!
    
    // MARK: private
    private let businessIndicator = UIActivityIndicatorView()
    private let cardRemoteScrollView = BusinessCardRemoteScrollView()
    private let businessCardIndicatorLabel = UILabel()
    private var viewModel: BusinessCardVMType
    
    init(viewModel: BusinessCardVMType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        setupBusinessIndicators()
        businessIndicator.startAnimating()
        
        viewModel.loadBusinessCard()
        
        viewModel.gotNetworkError = { error in
            self.businessIndicator.stopAnimating()
            self.setupEmptyView()
        }
        
        viewModel.gotContent = { content in
            self.cardRemoteScrollView.content = content
            self.businessIndicator.stopAnimating()
            self.setupView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupOverlay()
    }
    
    private func setupOverlay() {
        //Set up overlay for avatarImageView
        overlay = GDOverlay()
        
        overlay.arrowColor = UIColor(named: "academy")!
        overlay.boxBackColor = UIColor.clear
        overlay.boxBorderColor = UIColor.clear
        
        overlay.highlightView = true
        overlay.arrowWidth = 2.0
        overlay.headColor = UIColor.white
        overlay.headRadius = 6
        overlay.labelFont = UIFont.systemFont(ofSize: 14)
        overlay.labelColor = UIColor.white
        
        overlay.delegate = self
        
        overlay.lineType = LineType.line_arrow
        
        onSkipSignal()
    }
    
    // Overlay function
    func onSkipSignal() {
        if !UserDefaults.standard.bool(forKey: "firstLaunch") {
            overlay.drawOverlay(to: cardRemoteScrollView.avatarImageView, desc: "Zmáčkni mě!", isCircle: true)
            UserDefaults.standard.set(true, forKey: "firstLaunch")
            UserDefaults.standard.synchronize()
        } else {
            return
        }
    }
    
    private func setupBusinessIndicators() {
        view.addSubview(businessIndicator)
        businessIndicator.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, centerX: view.centerXAnchor, centerY: view.centerYAnchor)
        businessIndicator.color = UIColor(named: "academy")
    }
    
    private func setupView() {
        view.backgroundColor = .white
        view.addSubview(cardRemoteScrollView)
        cardRemoteScrollView.frame = view.bounds
        cardRemoteScrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Remove logout button from SuperView
        cardRemoteScrollView.logoutButton?.removeFromSuperview()
        
        //Set up Navigation Controller
        self.navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.title = cardRemoteScrollView.content?.name
    }
    
    private func setupEmptyView() {
        businessCardIndicatorLabel.text = "Zkontrolujte připojení"
        businessCardIndicatorLabel.textColor = UIColor(named: "academy")
        businessCardIndicatorLabel.font = UIFont.boldSystemFont(ofSize: 16)
        
        view.addSubview(businessCardIndicatorLabel)
        businessCardIndicatorLabel.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, centerX: view.centerXAnchor, centerY: view.centerYAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: businessCardIndicatorLabel.intrinsicContentSize.width, height: 44))
    }
}
