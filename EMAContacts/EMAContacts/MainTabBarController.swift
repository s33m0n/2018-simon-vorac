//
//  ViewController.swift
//  EMAContacts
//
//  Created by No Name on 25/03/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBarVC()
    }
    
    func setupTabBarVC() {
        UITabBar.appearance().tintColor = UIColor(named: "academy")
        
        let participantsVC = ParticipantsRemoteViewController(viewModel: ParticipantsVM())
        participantsVC.tabBarItem = UITabBarItem(title: "Seznam Účastníků",
                                                       image: UIImage(named: "ic-list"),
                                                       selectedImage: UIImage(named: "ic-list"))
        
       
        let accountRemoteVC = AccountRemoteViewController(viewModel: AccountVM(dataProvider: RemoteDataProvider()))
        accountRemoteVC.tabBarItem = UITabBarItem(title: "Účet",
                                            image: UIImage(named: "ic-account"),
                                            selectedImage: UIImage(named: "ic-account"))
        
        viewControllers = [UINavigationController(rootViewController: participantsVC), accountRemoteVC]
    }
}
