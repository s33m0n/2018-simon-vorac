//
//  LoginAuthView.swift
//  EMAContacts
//
//  Created by No Name on 25/03/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginRemoteViewController: UIViewController {
    
    var onLoginFinish: ((BusinessCardRemoteContent) -> Void)?
    var content: BusinessCardRemoteContent?
    
    //MARK: Private
    private var viewModel: LoginVMType
    
    private let businessIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    private lazy var dismissButton = makeDismissButton()
    private lazy var infoLabel = makeLabel()
    private lazy var nameTextField = makeInput(placeholder: "Zadejte přihlašovací email",
                                               returnKeyType: .next,
                                               secureEntry: false,
                                               keyboardType: .emailAddress)
    private lazy var passwordTextField = makeInput(placeholder: "Heslo",
                                                   returnKeyType: .go,
                                                   secureEntry: true,
                                                   keyboardType: .asciiCapable)
    private lazy var loginButton = makeButton(title: "Přihlásit")
    private lazy var stackView = UIStackView(arrangedSubviews: [nameTextField, passwordTextField])
    
    init() {
        viewModel = LoginVM(dataProvider: RemoteDataProvider())
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = .white
        [dismissButton, infoLabel, stackView, loginButton].forEach {  view.addSubview($0) }
        nameTextField.becomeFirstResponder()
        enable()
        setupAuthVC()
        
        viewModel.gotContent = { participant in
            self.businessIndicator.stopAnimating()
            UserDefaults.standard.set(participant.name, forKey: "ParticipantName")
            UserDefaults.standard.synchronize()
            self.enable()
            self.onLoginFinish?(participant)
            self.dismiss(animated: true, completion: nil)
        }
        
        viewModel.gotBadResponse = {
            self.businessIndicator.stopAnimating()
            self.infoLabel.text = "Váš email nebo heslo je nesprávné"
            self.shakeLogin()
            self.enable()
        }
        
        viewModel.gotNetworkError = { error in
            self.infoLabel.text = "Zkontrolujte vaše připojení"
            self.shakeLogin()
            self.enable()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
        passwordTextField.delegate = self
        setupTapGesture()
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y = -keyboardSize.height / 2
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        self.view.frame.origin.y = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func setupTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func handleTap() {
        view.endEditing(true)
    }
    
    private func setupAuthVC() {
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, centerX: view.centerXAnchor, centerY: view.centerYAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: -20), size: .zero)
        
        infoLabel.anchor(top: nil, leading: nil, bottom: nameTextField.topAnchor, trailing: nil, centerX: view.centerXAnchor, centerY: nil, padding: .init(top: 0, left: 0, bottom: 10, right: 0), size: .init(width: infoLabel.intrinsicContentSize.width, height: 44))
        nameTextField.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, centerX: nil, centerY: nil, padding: .init(top: 20, left: 20, bottom: 0, right: -20), size: .init(width: 0, height: 44))
        passwordTextField.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, centerX: nil, centerY: nil, padding: .init(top: 10, left: 20, bottom: 0, right: -20), size: .init(width: 0, height: 44))
        loginButton.anchor(top: stackView.bottomAnchor, leading: nil, bottom: nil, trailing: nil, centerX: stackView.centerXAnchor, centerY: nil, padding: .init(top: 20, left: 0, bottom: 0, right: 0), size: .init(width: 120, height: 60))
        dismissButton.anchor(top: nil, leading: nil, bottom: view.layoutMarginsGuide.bottomAnchor, trailing: nil, centerX: view.centerXAnchor, centerY: nil, padding: .init(top: 0, left: 0, bottom: -20, right: 0), size: .init(width: 44, height: 44))
    }
    
    private func setupBusinessIndicators() {
        view.addSubview(businessIndicator)
        businessIndicator.anchor(top: loginButton.bottomAnchor, leading: nil, bottom: nil, trailing: nil, centerX: view.centerXAnchor, centerY: nil, padding: .init(top: 20, left: 0, bottom: 0, right: 0), size: .zero)
        businessIndicator.color = UIColor(named: "academy")
    }
    
    private func makeDismissButton() -> UIButton {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "cancelButton"), for: .normal)
        button.imageView?.tintColor = UIColor(named: "academy")
        button.addTarget(self, action: #selector(onDismissClick), for: .touchUpInside)
        return button
    }
    
    private func makeButton(title: String) -> UIButton {
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 22)
        button.layer.cornerRadius = 6
        button.backgroundColor = UIColor(named: "academy")
        button.addTarget(self, action: #selector(onLoginClick), for: .touchUpInside)
        return button
    }
    
    private func makeInput(placeholder: String, returnKeyType: UIReturnKeyType, secureEntry: Bool, keyboardType: UIKeyboardType) -> UITextField {
        let textField = UITextField()
        textField.placeholder = placeholder
        textField.textAlignment = .center
        textField.tintColor = UIColor(named: "academy")
        textField.borderStyle = .roundedRect
        textField.keyboardType = keyboardType
        textField.returnKeyType = returnKeyType
        textField.isSecureTextEntry = secureEntry
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.autocapitalizationType = UITextAutocapitalizationType.none
        textField.clearButtonMode = UITextFieldViewMode.whileEditing
        textField.enablesReturnKeyAutomatically = true
        return textField
    }
    
    private func makeLabel() -> UILabel {
        let label = UILabel()
        label.textColor = UIColor(named: "academy")
        label.font = UIFont.boldSystemFont(ofSize: 11)
        return label
    }
    
    // OnLoginClick func
    @objc private func onLoginClick() {
        passwordTextField.resignFirstResponder()
        
        //Disable textFields & button
        disable()
        
        //Login with email & password
        login()
    }
    
    // OnDismissClick func
    @objc private func onDismissClick() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func login() {
        setupBusinessIndicators()
        if let nameTextField = nameTextField.text, let passwordTextField = passwordTextField.text {
            businessIndicator.startAnimating()
            viewModel.login(email: nameTextField, password: passwordTextField)
        }
    }
    
    func enable() {
        nameTextField.isEnabled = true
        passwordTextField.isEnabled = true
        loginButton.isEnabled = true
        nameTextField.text = ""
        passwordTextField.text = ""
    }
    
    func disable() {
        nameTextField.isEnabled = false
        passwordTextField.isEnabled = false
        loginButton.isEnabled = false
    }
    
    func shakeLogin() {
        //Using extensions
        nameTextField.shake(count: 4, for: 0.2, withTranslation: 10)
        passwordTextField.shake(count: 4, for: 0.2, withTranslation: 10)
        infoLabel.isHidden = false
    }
}

// MARK: UITextFieldDelegate
extension LoginRemoteViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            nameTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            passwordTextField.resignFirstResponder()
            onLoginClick()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        infoLabel.isHidden = true
        businessIndicator.stopAnimating()
    }
}
