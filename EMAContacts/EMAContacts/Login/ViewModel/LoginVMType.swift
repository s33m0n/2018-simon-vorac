//
//  LoginVMType.swift
//  EMAContacts
//
//  Created by No Name on 18/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

protocol LoginVMType {
    
    var gotContent: ((BusinessCardRemoteContent) -> Void)? { get set }
    var gotBadResponse: (() -> Void)? { get set }
    var gotNetworkError: ((Error?) -> Void)? { get set }
    
    func login(email: String, password: String) -> Void
}
