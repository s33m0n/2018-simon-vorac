//
//  LoginVM.swift
//  EMAContacts
//
//  Created by No Name on 18/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

enum NetworkStatus {
    case success
    case networkError
    case badResponse
}

class LoginVM: LoginVMType {
    
    var gotContent: ((BusinessCardRemoteContent) -> Void)?
    var gotBadResponse: (() -> Void)?
    var gotNetworkError: ((Error?) -> Void)?
    
    private var dataProvider: RemoteDataProviderType
    
    init(dataProvider: RemoteDataProviderType) {
        self.dataProvider = dataProvider
    }
    
    func handleResponse(_ response: HTTPURLResponse) -> NetworkStatus {
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .badResponse
        case 501...599: return .networkError
        default: return .badResponse
        }
    }
    
    //POST Request with email & password
    func login(email: String, password: String) -> Void {
        dataProvider.login(email: email, password: password) { (credentials, response, error) in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.gotNetworkError?(error)
                }
            }
            
            guard let urlResponse = response as? HTTPURLResponse else { return }
            switch self.handleResponse(urlResponse) {
                
            case .success:
                guard let credentials = credentials else { return }
                UserDefaults.standard.set(credentials.accountId, forKey: "AccountID")
                UserDefaults.standard.set(credentials.accessToken, forKey: "AccessToken")
                
                //GET Request with AccountID & AccessToken
                //If response == 200 OK proceed with login
                self.dataProvider.account(id: credentials.accountId, accessToken: credentials.accessToken, completion: { content in
                    guard let content = content else { return }
                    self.gotContent?(content)
                })
                
            case .networkError:
                DispatchQueue.main.async {
                    self.gotNetworkError?(error)
                }
                
            case .badResponse:
                DispatchQueue.main.async {
                    self.gotBadResponse?()
                }
            }
        }
    }
}
