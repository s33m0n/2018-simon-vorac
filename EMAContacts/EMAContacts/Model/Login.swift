//
//  Login.swift
//  EMAContacts
//
//  Created by No Name on 12/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

class Login: Encodable {
    let email: String
    let password: String
    
    init(_ email: String,_ password: String) {
        self.email = email
        self.password = password
    }
}
