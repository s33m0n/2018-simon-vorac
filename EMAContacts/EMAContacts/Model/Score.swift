//
//  Score.swift
//  EMAContacts
//
//  Created by No Name on 12/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

public class Score: Decodable {
    let value: Int
    let emoji: String
    var name: String {
        return emoji + " Skóre"
    }
    
    init(value: Int, emoji: String) {
        self.value = value
        self.emoji = emoji
    }
}
