//
//  Participants.swift
//  EMAContacts
//
//  Created by No Name on 12/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

class Participant: Decodable {
    let id: Int
    let name: String
    let icon: String
    let imageUrl: String?
    let scores: [Score]
    
    init(id: Int, name: String, icon: String, imageUrl: String?, scores: [Score]) {
        self.id = id
        self.name = name
        self.icon = icon
        self.imageUrl = imageUrl
        self.scores = scores
    }
}




