//
//  AccountViewController.swift
//  EMAContacts
//
//  Created by No Name on 08/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import UIKit
import LocalAuthentication

class AccountRemoteViewController: UIViewController {
    
    private let loginRemoteVC = LoginRemoteViewController()
    private let cardScrollView = BusinessCardRemoteScrollView()
    private let businessIndicator = UIActivityIndicatorView()
    private var loginButton: UIButton?
    private let viewModel: AccountVMType
    
    init(viewModel: AccountVMType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        setupContent()
        
        //Set from LoginAuthRemoteVC with (participant) content
        loginRemoteVC.onLoginFinish = { content in
            UserDefaults.standard.set(true, forKey: "isLoggedIn")
            self.loginButton?.removeFromSuperview()
            self.cardScrollView.content = content
            self.view.addSubview(self.cardScrollView)
            self.cardScrollView.frame = (self.view.bounds)
            self.cardScrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }
        
        cardScrollView.dismiss = {
            self.view.moveAnimation(withDuration: 0.5, withAxis: .y, withAnimStepCount: 100, completion: { _ in
                self.setupContent()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loginButton?.zoomAnimation(withDuration: 0.3, withZoomLevel: 1.25, completion: nil)
    }
    
    private func setupContent() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == true {
            loginButton?.removeFromSuperview()
            authenticateWithBiometrics()
        } else {
            cardScrollView.removeFromSuperview()
            setupLoginButton()
        }
    }
    
    private func setupBusinessIndicators() {
        view.addSubview(businessIndicator)
        businessIndicator.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, centerX: view.centerXAnchor, centerY: view.centerYAnchor)
        businessIndicator.color = UIColor(named: "academy")
    }
    
    private func setupLoginButton() {
        let button = makeButton()
        view.addSubview(button)
        button.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, centerX: view.centerXAnchor, centerY: view.centerYAnchor, padding: .zero, size: .init(width: 120, height: 60))
        
        self.loginButton = button
    }
    
    private func makeButton() -> UIButton {
        let button = UIButton(type: .roundedRect)
        button.setTitle("Přihlásit", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        button.layer.cornerRadius = 6
        button.backgroundColor = UIColor(named: "academy")
        button.addTarget(self, action: #selector(onLoginClick), for: .touchUpInside)
        return button
    }
    
    @objc private func onLoginClick() {
        self.present(loginRemoteVC, animated: true, completion: nil)
    }
}

// MARK: Biometric Authentication
extension AccountRemoteViewController {
    
    private func authenticateWithBiometrics() {
        setupBusinessIndicators()
        businessIndicator.startAnimating()
        
        viewModel.authenticateWithBiometrics { [weak self] (content, error) in
            DispatchQueue.main.async {
                if let content = content {
                    DispatchQueue.main.async {
                        self?.businessIndicator.stopAnimating()
                        self?.cardScrollView.content = content
                        self?.view.addSubview((self?.cardScrollView)!)
                        self?.cardScrollView.frame = (self?.view.bounds)!
                        self?.cardScrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                    }
                } else if error != nil {
                    self?.businessIndicator.stopAnimating()
                    self?.cardScrollView.removeFromSuperview()
                    self?.setupLoginButton()
                } else {
                    self?.businessIndicator.stopAnimating()
                    self?.cardScrollView.removeFromSuperview()
                    self?.setupLoginButton()
                    self?.present((self?.loginRemoteVC)!, animated: true, completion: nil)
                }
            }
        }
    }
}
