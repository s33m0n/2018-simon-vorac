//
//  AccountVMType.swift
//  EMAContacts
//
//  Created by No Name on 18/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

protocol AccountVMType {
    func account(id: Int, accessToken: String, completion: @escaping (_ participant: BusinessCardRemoteContent?) -> Void)
    func authenticateWithBiometrics(completion: @escaping (_ content: BusinessCardRemoteContent?, _ error: Error?) -> Void)
}
