//
//  AccountVM.swift
//  EMAContacts
//
//  Created by No Name on 18/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation
import LocalAuthentication

class AccountVM: AccountVMType {

    private let dataProvider: RemoteDataProviderType
    
    init(dataProvider: RemoteDataProviderType) {
        self.dataProvider = dataProvider
    }
    
    //Get participant's content
    func account(id: Int, accessToken: String, completion: @escaping (BusinessCardRemoteContent?) -> Void) {
        // Not using [weak self] as it's not needed at this moment
        dataProvider.account(id: id, accessToken: accessToken) { participant in
            guard let participant = participant else { return }
            completion(participant)
        }
    }
}

// MARK: Biometric Authentication ViewModel
extension AccountVM {
    func authenticateWithBiometrics(completion: @escaping (BusinessCardRemoteContent?, Error?) -> Void) {
        let participantName = UserDefaults.standard.string(forKey: "ParticipantName")
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Přihlásit se"
        localAuthenticationContext.localizedCancelTitle = "Zrušit"
        var authError: NSError?
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "\(participantName!), přihlašte se ke svému účtu") { (success, error) in
                if success {
                    if let accessToken = UserDefaults.standard.string(forKey: "AccessToken") {
                        self.account(id: UserDefaults.standard.integer(forKey: "AccountID"), accessToken: accessToken, completion: { content in
                            UserDefaults.standard.set(true, forKey: "authenticatedWithBiometrics")
                            completion(content, error)
                        })
                    }
                } else {
                    guard let error = error else { return }
                    completion(nil, nil)
                    print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                }
            }
        } else {
            guard let error = authError else { return }
            completion(nil, error)
            print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
        }
    }
    
    // Biometric's error handling
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        return message
    }
    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.biometryLockout.rawValue:
                message = "Too many failed attempts."
            case LAError.biometryNotAvailable.rawValue:
                message = "TouchID is not available on the device"
            case LAError.biometryNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
            default:
                message = "Did not find error code on LAError object"
            }
        }
        return message;
    }
}
