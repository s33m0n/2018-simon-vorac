//
//  DataProviderType.swift
//  EMAContacts
//
//  Created by No Name on 12/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

protocol DataProviderType {
    func loadParticipants(completion: @escaping (_ participants: [Participant]?, _ error: Error?) -> Void)
    func login(email: String, password: String, completion: @escaping (_ accountCredentials: AccountCredentials?,_ response: URLResponse?,_ error: Error?) -> Void)
    func account(id: Int, accessToken: String, completion: @escaping (_ participant: BusinessCardRemoteContent?) -> Void)
    func loadParticipant(id: Int, completion: @escaping (_ participants: BusinessCardRemoteContent?, _ error: Error?) -> Void)
}
