//
//  RemoteDataProviderType.swift
//  EMAContacts
//
//  Created by No Name on 12/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

enum Endpoint: String {
    case participants = "/api/participants?sort=asc"
    case participant = "/api/participant/"
    case login = "/api/login"
    case account = "/api/account/"
}

protocol RemoteDataProviderType: DataProviderType {
    var baseUrl: String { get }
    func getUrl(for endpoint: Endpoint) -> URL
}

extension RemoteDataProviderType {
    
    var baseUrl: String {
        return "http://emarest.cz.mass-php-1.mit.etn.cz"
    }
    
    func getUrl(for endpoint: Endpoint) -> URL {
        return URL(string: baseUrl + endpoint.rawValue)!
    }
}
