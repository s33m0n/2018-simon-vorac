//
//  RemoteDataProvider.swift
//  EMAContacts
//
//  Created by No Name on 12/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

class RemoteDataProvider: RemoteDataProviderType {
    
    func login(email: String, password: String, completion: @escaping (AccountCredentials?, URLResponse?, Error?) -> Void) {
        let login = Login(email, password)
        let data = try? JSONEncoder().encode(login)
        
        let url = getUrl(for: .login)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = data
        
        let urlSession = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let data = data {
                let model = try? JSONDecoder().decode(AccountCredentials.self, from: data)
                DispatchQueue.main.sync {
                    completion(model, response, error)
                }
            }
            completion(nil, response, error)
        }
        urlSession.resume()
    }
    
    func account(id: Int, accessToken: String, completion: @escaping (BusinessCardRemoteContent?) -> Void) {
        let url = getUrl(for: .account)
        let urlAccount = URL(string: "\(url)\(id)")
        guard let urlWithToken = urlAccount else { return }
        var urlRequest = URLRequest(url: urlWithToken)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue(accessToken, forHTTPHeaderField: "AccessToken")
        
        let urlSession = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let data = data {
                let model = try? JSONDecoder().decode(BusinessCardRemoteContent.self, from: data)
                DispatchQueue.main.sync {
                    completion(model)
                }
            }
        }
        urlSession.resume()
    }
    
    func loadParticipants(completion: @escaping (_ participants: [Participant]?, Error?) -> Void) {
        let url = getUrl(for: .participants)
        var urlRequest = URLRequest(url: url)
        urlRequest.timeoutInterval = 10
        
        let urlSession = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let data = data {
                let model = try? JSONDecoder().decode([Participant].self, from: data)
                DispatchQueue.main.sync {
                    completion(model, nil)
                }
            }
            if error != nil {
                completion(nil, error)
            }
        }
        urlSession.resume()
    }
    
    func loadParticipant(id: Int, completion: @escaping (_ participant: BusinessCardRemoteContent?, Error?) -> Void) {
        let url = getUrl(for: .participant)
        let urlID = URL(string: "\(url)\(id)")
        
        guard let urlWithID = urlID else { return }
        let urlSession = URLSession.shared.dataTask(with: urlWithID) { (data, response, error) in
            if let data = data {
                let model = try? JSONDecoder().decode(BusinessCardRemoteContent.self, from: data)
                DispatchQueue.main.sync {
                    completion(model, nil)
                }
            }
            if error != nil {
                completion(nil, error)
            }
        }
        urlSession.resume()
    }
}
