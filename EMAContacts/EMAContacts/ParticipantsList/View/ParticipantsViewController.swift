//
//  ParticipantsRemoteViewController.swift
//  EMAContacts
//
//  Created by No Name on 09/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import UIKit

class ParticipantsRemoteViewController: UIViewController {
    
    // MARK: constants
    let CELLID = "Cell"
 
    // MARK: private
    private var viewModel: ParticipantsVMType
    private var tableView = UITableView()
    private var refreshControl: UIRefreshControl!
    private var customView: UIView!
    private var labelsArray = [UILabel]()
    
    // ActivityIndicator
    private let participantIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    private let participantIndicatorLabel = UILabel()
    private let reloadButton = UIButton(type: .custom)
    
    // MARK: Init
    init(viewModel: ParticipantsVMType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        setupParticipantIndicators()
        reloadButton.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        participantIndicator.startAnimating()
        
        viewModel.gotNetworkError = { [weak self] error in
            self?.participantIndicator.stopAnimating()
            self?.participantIndicatorLabel.textColor = UIColor(named: "academy")
            self?.participantIndicatorLabel.text = "Zkontrolujte vaše připojení"
            self?.reloadButton.isHidden = false
        }
        
        viewModel.didUpdateModel = { [weak self] model in
            self?.participantIndicator.stopAnimating()
            self?.setupTableView()
            self?.tableView.reloadData()
            self?.setupParticipantsNC()
        }
        
        viewModel.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.indexPathsForSelectedRows?.forEach {
            tableView.deselectRow(at: $0, animated: true)
        }
    }
}

// MARK: Functions
extension ParticipantsRemoteViewController {
    // NavigationController setup
    func setupParticipantsNC() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = UIColor(named: "academy")
        navigationController?.navigationBar.topItem?.title = "EMA 2018"
    }
    
    // TableView setup
    func setupTableView() {
        let tableView = UITableView()
        view.addSubview(tableView)
        tableView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, centerX: nil, centerY: nil)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ParticipantsCell.self, forCellReuseIdentifier: CELLID)
        self.tableView = tableView
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = .clear
        tableView.addSubview(refreshControl)
        loadCustomRefreshContents()
    }
    
    // ParticipantIndicators (indicator, label, reloadButton)
    func setupParticipantIndicators() {
        view.addSubview(participantIndicator)
        participantIndicator.anchor(top: nil, leading: nil, bottom: nil, trailing: nil, centerX: view.centerXAnchor, centerY: view.centerYAnchor)
        participantIndicator.color = UIColor(named: "academy")
        
        view.addSubview(participantIndicatorLabel)
        participantIndicatorLabel.text = "Nahrávám účastníky..."
        participantIndicatorLabel.font = UIFont.boldSystemFont(ofSize: 16)
        participantIndicatorLabel.anchor(top: participantIndicator.bottomAnchor, leading: nil, bottom: nil, trailing: nil, centerX: participantIndicator.centerXAnchor, centerY: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: participantIndicatorLabel.intrinsicContentSize.width, height: 44))
        
        view.addSubview(reloadButton)
        reloadButton.setImage(UIImage(named: "reloadButton"), for: .normal)
        reloadButton.setImage(UIImage(named: "reloadButtonPressed"), for: .highlighted)
        reloadButton.imageView?.tintColor = UIColor(named: "academy")
        reloadButton.anchor(top: participantIndicatorLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil, centerX: participantIndicatorLabel.centerXAnchor, centerY: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 44, height: 44))
        reloadButton.addTarget(self, action: #selector(reloadData), for: .touchUpInside)
    }
    
    @objc func reloadData() {
        viewModel.loadData()
    }
}

// MARK: - Table view delegate & data source
extension ParticipantsRemoteViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.modelForSection(section).header
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return viewModel.modelForSection(section).footer
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewModel.modelForRow(inSection: indexPath.section, atIdx: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: CELLID, for: indexPath) as! ParticipantsCell
        cell.addSubViewsAndlayout()
        
        switch model {
        case let .participant(participant):
            cell.setupCell(with: participant, viewModel: ParticipantsVM())
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = viewModel.modelForRow(inSection: indexPath.section, atIdx: indexPath.row)
        switch row {
        case let .participant(participant):
            let businessCardVC = BusinessCardRemoteViewController(viewModel: BusinessCardVM(id: participant.id))
            pushVC(businessCardVC)
        }
    }
    
    private func pushVC(_ vc: BusinessCardRemoteViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    // MARK: ANIMATION
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if refreshControl.isRefreshing {
            for label in labelsArray {
                label.zoomAnimation(withDuration: 0.7, withZoomLevel: 1.2) { _ in
                    label.zoomAnimation(withDuration: 0.7, withZoomLevel: 0.8, completion: nil)
                }
            }
            viewModel.loadData()
        }
    }
}

// MARK: Custom Refresh Controller
extension ParticipantsRemoteViewController {
    func loadCustomRefreshContents() {
        let refreshContents = Bundle.main.loadNibNamed("RefreshContents", owner: self, options: nil)
        customView = refreshContents?[0] as! UIView
        customView.frame = refreshControl.bounds
        for i in 1...customView.subviews.count {
            labelsArray.append(customView.viewWithTag(i) as! UILabel)
        }
        refreshControl.addSubview(customView)
    }
}


