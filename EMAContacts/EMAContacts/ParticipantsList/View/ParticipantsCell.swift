//
//  ParticipantsCell.swift
//  EMAContacts
//
//  Created by No Name on 26/03/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import UIKit
import Kingfisher

public class ParticipantsCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(named: "academy")?.withAlphaComponent(0.3)
        selectedBackgroundView = backgroundView
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addSubViewsAndlayout() {
        self.accessoryType = .disclosureIndicator
    }
    
    func setupCell(with participant: Participant, viewModel: ParticipantsVMType) {
        self.textLabel?.text = participant.name
        
        //Get participant's image from url and set it using Kingfisher
        guard let imageUrl = participant.imageUrl else { return }
        let url = URL(string: imageUrl)
        self.imageView?.kf.setImage(with: url, completionHandler: { [weak self] (_, _, _, _) in
            self?.setNeedsLayout()
        })
        
        self.detailTextLabel?.attributedText = viewModel.getDetailText(for: participant)
    }
}
