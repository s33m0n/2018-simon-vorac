//
//  ParticipantVM.swift
//  EMAContacts
//
//  Created by No Name on 17/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import Foundation

protocol ParticipantsVMType {
    typealias Section = (header: String?, rows: [Row], footer: String?)
    typealias Model = [Section]
    
    var model: Model { get }
    var didUpdateModel: ((Model) -> Void)? { get set }
    var gotNetworkError: ((Error?) -> Void)? { get set }
    
    func numberOfSections() -> Int
    func numberOfRows(inSection: Int) -> Int
    func modelForSection(_ section: Int) -> Section
    func modelForRow(inSection: Int, atIdx: Int) -> Row
    
    func loadData()
    func getDetailText(for: Participant) -> NSMutableAttributedString
}

extension ParticipantsVMType {
    func numberOfSections() -> Int {
        return model.count
    }
    
    func numberOfRows(inSection: Int) -> Int {
        return model[inSection].rows.count
    }
    
    func modelForSection(_ section: Int) -> ParticipantsVMType.Section {
        return model[section]
    }
    
    func modelForRow(inSection: Int, atIdx: Int) -> Row {
        return model[inSection].rows[atIdx]
    }
}
