//
//  ParticipantsVM.swift
//  EMAContacts
//
//  Created by No Name on 17/04/2018.
//  Copyright © 2018 s33m0n. All rights reserved.
//

import UIKit

class ParticipantsVM: ParticipantsVMType {
    
    var gotNetworkError: ((Error?) -> Void)?
    
    private(set) var model: [ParticipantsVMType.Section] = [] {
        didSet {
            didUpdateModel?(model)
        }
    }
    
    var didUpdateModel: ((ParticipantsVMType.Model) -> Void)?
    
    private let dataProvider: DataProviderType
    
    // Init
    init() {
        self.dataProvider = RemoteDataProvider()
    }
    
    func loadData() {
        dataProvider.loadParticipants { [weak self] (participants, error) in
            guard let participants = participants else {
                DispatchQueue.main.async {
                    self?.gotNetworkError?(error)
                }
                return
            }
            let rowForParticipant = participants.map { Row.participant($0) }
            self?.model = [Section(header: "Účastníci", rows: rowForParticipant, footer: nil)]
        }
    }
    
    func getTopScores(for participant: Participant) -> [Score] {
        let sortedScores = participant.scores
            .filter{ $0.value != 0 }
            .sorted{ $0.value > $1.value}
        return Array(sortedScores.prefix(4))
    }
    
    func getDetailText(for participant: Participant) -> NSMutableAttributedString {
        return getTopScores(for: participant).reduce(NSMutableAttributedString()) { (final, current) in
            final.append(NSAttributedString(string: current.emoji, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 24)]))
            final.append(NSAttributedString(string: " \(current.value)   ", attributes: [
                NSAttributedStringKey.font : UIFont.systemFont(ofSize: 13),
                NSAttributedStringKey.baselineOffset: 2,
                NSAttributedStringKey.foregroundColor: UIColor.gray
                ]))
            return final
        }
    }
}
